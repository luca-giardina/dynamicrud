# Getting started #
Dynamic CRUD application (DynamiCrud) can be used for standard table operations.
It will have dynamic routes according to application Models for GET, POST, PUT and DELETE HTTP's protocol. The component uses the models configuration in laravel/lumen and performes checks using the protected arguments.



# Installation #

**REQUIRE Eloquent and Facades**

uncomment 


```
$app->withFacades();
$app->withEloquent();

```


and add the DynamicrudServiceProvider


```

$app->register(Giardina\Dynamicrud\DynamicrudServiceProvider::class);
```



in

**bootstrap/app.php**



# Routing #

The application, by default, will transform snake_case routes to search a StudlyCase Model in Models IF **no "MODELS" env var is set**.

Routes Example:
```
1) $HOST / user 
2) $HOST / user_more
3) $HOST / another_big_route
```

will try to use a Model named:
```
1) App\User
2) App\UserMore
3) App\AnotherBigRoute
```

## Custom Route ##

Create your routes configuration using "MODELS" env var in .env file

Example:


```

MODELS={"user":"App\\CustomUser", "another_route":"App\\AnotherModel"}
```


# Model Setup #


```
#!php

class CustomUser extends Model
{
    use SoftDeletes;
 
    protected $table = 'user';
    protected $primaryKey = 'id';
 
    protected $fillable = [
        "title",
        "name",
        "surname",
        "display_name",
        "passport",
        "nationality_id",
        "image"
    ];
 
    protected $guarded = ['id'];
     
    public $rules = [
        "nationality_id" => 'required|integer',
        "passport" => 'string',
        "name" => 'required|string'
    ];
}
```

By default the application have registered routes for GET, POST, PUT, DELETE

** GET: ** $HOST / user

** POST: ** $HOST / user

** PUT: ** $HOST / user / { id }

** DELETE: ** $HOST / user / { id }


Every route have a wildcard on the / { id } part, so the POST will works for  $HOST / user / { id } as well. The ID part will be ignored (if POST protocol)

The DELETE will work for  $HOST / user  too if you pass the $primaryKey (person_id) in the request body / or error returned.


The GET protocol will response with a list of ALL the elements if no ID is passed.