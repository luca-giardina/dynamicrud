<?php

namespace Giardina\Dynamicrud\Controllers;


use Illuminate\Http\Request;
use Giardina\Dynamicrud\Controllers\DynamicrudController;

class DeleteController extends DynamicrudController
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Safety checks for the request
     *
     * @return bool
     */

    protected function SafeChecks ( $id, $oModel, $callname )
    {
        $this->SafeChecksMsg = [];

        /* Input checks */

        if ( !$id )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_DELETE_ID_NOT_FOUND', 422 );
            return false;
        }
        else
        {
            if ( !is_numeric( $id ) )
            {
                $this->SafeChecksMsg = $this->sendResponse( $callname . '_DELETE_ID_BAD_FORMAT', 422 );
                return false;
            }
        }
        /* DB checks */

        $oModel = $oModel->find( $id );

        if ( !$oModel )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_DELETE_NOT_FOUND', 404 );
            return false;
        }

        return $oModel;

    }



    /**
     * Delete App\$model
     *
     * @return [
            "status": integer,
            "result": App\$model\id | false [ success | fail ]
        ]
     */

    public function Dynamic( Request $request, $model = null, $id = null )
    {
        if ( !$this->SafeModelChecks( $model) )
            return $this->SafeChecksMsg;


        $oModel = new $this->models[ $model ];


        $IdAttribute = $oModel->getKeyName();


        $id = $request->input( $IdAttribute ) ?? $id;

        $oSingleModel = $this->SafeChecks( $id, $oModel, strtoupper( $model ) );

        if ( !$oSingleModel )
            return $this->SafeChecksMsg;


        if ( $oSingleModel->delete() )
        {
            return $this->sendResponse( $oSingleModel->$IdAttribute );
        }

        return $this->sendResponse( false, 422 );
    }

}

