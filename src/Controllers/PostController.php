<?php

namespace Giardina\Dynamicrud\Controllers;


use Illuminate\Http\Request;
use Giardina\Dynamicrud\Controllers\DynamicrudController;
use Exception;

class PostController extends DynamicrudController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Save NEW App\$model information.
     *
     * @return [
            "status": integer,
            "result": App\$model\id | false [ success | fail ]
        ]
     */

    public function Dynamic( Request $request, $model = null, $id = null )
    {
        if ( !$this->SafeModelChecks( $model) )
            return $this->SafeChecksMsg;


        if( empty( $request->all() ) )
        {
            return $this->sendResponse( strtoupper($model) . '_POST_EMPTY', 400 );
        }


        $oModel = new $this->models[ $model ];
        $IdAttribute = $oModel->getKeyName();

        $ValidationResult = $this->ModelValidate( $request->all(), $oModel );

        if( $ValidationResult === true )
        {
            try
            {
                $saved = $oModel->isClean() || $oModel->save();
            }
            catch(Exception $e)
            {
                return $this->sendResponse( $e->getMessage(), 422 );
            }

            if ( $saved )
            {
                return $this->sendResponse( $oModel->$IdAttribute ?? true );
            }

            return $this->sendResponse( false, 500 );
        }

        return $this->sendResponse( $ValidationResult, 422 );
    }


}


