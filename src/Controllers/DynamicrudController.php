<?php

namespace Giardina\Dynamicrud\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Exception;

class DynamicrudController extends Controller {

    protected $models;

    protected $SafeChecksMsg;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = json_decode( env('MODELS'), true );
    }




    /**
     * Safety checks for the input request
     *
     * @return bool
     */

    protected function SafeModelChecks( $model )
    {
        if ( !$model )
        {
            $this->SafeChecksMsg = $this->sendResponse( 'APP_TABLE_NOT_SET', 400 );
            return false;
        }


        if( !isset( $this->models[ $model ] ))
        {
            // this is a smart algo, maybe you don't want to use config? uhuh?
            try
            {
                $MyModel = 'App\\' . studly_case($model);
                $ModelExists = new $MyModel ?? false;
            }
            catch(Exception $e)
            {
                $this->SafeChecksMsg = $this->sendResponse( 'APP_MODEL_NOT_FOUND', 404 );
            }

            if( !$ModelExists )
            {
                $this->SafeChecksMsg = $this->sendResponse( 'APP_TABLE_NOT_FOUND', 404 );
                return false;
            }
            
            $this->models[ $model ] = $MyModel;
        }

        return true;
    }


    /**
     * Check Validation for the input model
     *
     * @return bool
     */

    protected function ModelValidate( $data = [], $oModel )
    {
        try
        {
            $oModel->fill($data);
        }
        catch(Exception $e)
        {
            return false;
        }

        $oValidator = Validator::make($oModel->toArray(), $oModel->rules);

        if( empty($oModel->rules) || $oValidator->passes() )
            return true;
        else
            return $oValidator->errors()->all();
    }


    /* Custom response */

    protected function sendResponse ( $result, $status = 200 )
    {
        return [
            'status' => $status,
            'result' => $result
        ];
    }
}
