<?php

namespace Giardina\Dynamicrud\Controllers;


use Illuminate\Http\Request;
use Giardina\Dynamicrud\Controllers\DynamicrudController;
use Exception;

class PutController extends DynamicrudController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Safety checks for the request
     *
     * @return bool
     */

    protected function SafeChecks ( $request, $id, $oModel, $callname )
    {
        $this->SafeChecksMsg = [];

        /* Input checks */

        if( empty( $request->all() ) )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_PUT_EMPTY', 400 );
            return false;
        }

        if ( !$id )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_PUT_ID_NOT_FOUND', 404 );
            return false;
        }
        else
        {
            if ( !is_numeric( $id ) )
            {
                $this->SafeChecksMsg = $this->sendResponse( $callname . '_PUT_ID_BAD_FORMAT', 422 );
                return false;
            }
        }
        /* DB checks */

        $oModel = $oModel->find( $id );

        if ( !$oModel )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_PUT_NOT_FOUND', 404 );
            return false;
        }

        return $oModel;

    }


    /**
     * Update App\$model information.
     *
     * @return [
            "status": integer,
            "result": App\$model\id | false [ success | fail ]
        ]
     */


    public function Dynamic( Request $request, $model = null, $id = null )
    {
        if ( !$this->SafeModelChecks( $model) )
            return $this->SafeChecksMsg;

        $oModel = new $this->models[ $model ];

        $IdAttribute = $oModel->getKeyName();

        $id = $request->input( $IdAttribute ) ?? $id;

        $oSingleModel = $this->SafeChecks( $request, $id, $oModel, strtoupper( $model ) );

        if ( !$oSingleModel )
            return $this->SafeChecksMsg;

        $ValidationResult = $this->ModelValidate( $request->all(), $oSingleModel );

        if( $ValidationResult === true )
        {
            try
            {
                $updated = $oSingleModel->isClean() || $oSingleModel->update();
            }
            catch(Exception $e)
            {
                return $this->sendResponse( $e->getMessage(), 422 );
            }

            if ( $updated )
            {
                return $this->sendResponse( $oSingleModel->$IdAttribute );
            }

            return $this->sendResponse( false, 500 );
        }

        return $this->sendResponse( $ValidationResult, 422 );
    }

}


