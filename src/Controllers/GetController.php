<?php

namespace Giardina\Dynamicrud\Controllers;

use Illuminate\Http\Request;
use Giardina\Dynamicrud\Controllers\DynamicrudController;

class GetController extends DynamicrudController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Safety checks for the request
     *
     * @return Class | false
     */

    private function SafeChecks ( $id, $oModel, $callname )
    {
        $this->SafeChecksMsg = [];

        /* Input checks */

        if ( !$id )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_GET_ID_NOT_FOUND', 422 );
            return false;
        }
        else
        {
            if ( !is_numeric( $id ) )
            {
                $this->SafeChecksMsg = $this->sendResponse( $callname . '_GET_ID_BAD_FORMAT', 422 );
                return false;
            }
        }
        /* DB checks */

        $oModel = $oModel->find( $id );

        if ( !$oModel )
        {
            $this->SafeChecksMsg = $this->sendResponse( $callname . '_GET_NOT_FOUND', 404 );
            return false;
        }

        return $oModel;

    }


    /**
     * Retrive App\$model information.
     *
     * @return [ 
            "status": integer,
            "result": json array list of App\$model | error_string [ success | fail ]
        ]
     */

    public function Dynamic( Request $request, $model = null, $id = null )
    {
        if ( !$this->SafeModelChecks( $model) )
            return $this->SafeChecksMsg;

        
        $oModel = new $this->models[ $model ];

        if ( !$id )
        {
            return $this->sendResponse( $oModel->all() );
        }
        
        $oSingleModel = $this->SafeChecks( $id, $oModel, strtoupper( $model ) );

        if ( !$oSingleModel )
            return $this->SafeChecksMsg;

        return $this->sendResponse( $oSingleModel );
    }

}

