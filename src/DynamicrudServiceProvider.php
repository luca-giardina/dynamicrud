<?php

namespace Giardina\Dynamicrud;

use Illuminate\Support\ServiceProvider;

class DynamicrudServiceProvider extends ServiceProvider
{

   /**
    * Bootstrap the application services.
    *
    * @return void
    */
	public function boot()
	{
		//
		
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        include __DIR__.'/Routes/routes.php';

        $this->app->make('Giardina\Dynamicrud\Controllers\DynamicrudController'); 
        $this->app->make('Giardina\Dynamicrud\Controllers\GetController'); 
        $this->app->make('Giardina\Dynamicrud\Controllers\PostController'); 
        $this->app->make('Giardina\Dynamicrud\Controllers\PutController'); 
        $this->app->make('Giardina\Dynamicrud\Controllers\DeleteController'); 
    }
}
